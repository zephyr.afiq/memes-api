<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $meme_img
 * @property string $meme_text
 * @property int $page
 * @property int $request_count
 * @property string $created_at
 * @property string $updated_at
 */
class Meme extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['meme_img', 'meme_text', 'page', 'request_count', 'created_at', 'updated_at'];

}

<?php

namespace App\Http\Controllers;

use App\Meme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public function prefixArray($data){

        $result = []; // set an empty array

        foreach ($data as $datum) {

            $array = [
                'id' => $datum->id,
                'name' => $datum->meme_text,
                'url' => $datum->meme_img,
                'page' => $datum->page,
                'requestCount' => $datum->request_count
            ];

            array_push($result, $array);
        }

        return $result;
    }

    public function index()
    {

        $data = Meme::all(); // select all meme data

        $result = $this->prefixArray($data);

        return response()->json($result);
    }

    public function show($id)
    {
        $data = Meme::find($id); // select all meme data

        $data->update([
            'request_count' => $data->request_count + 1
        ]);

        $array = [
            'id' => $data->id,
            'name' => $data->meme_text,
            'url' => $data->meme_img,
            'page' => $data->page,
            'requestCount' => $data->request_count
        ];

        return response()->json($array);
    }

    public function page($page = 1)
    {

        $data = Meme::where('page', $page)->get(); // select meme data by page
        $result = $this->prefixArray($data);

        return response()->json($result);
    }

    public function popular()
    {
        $data = Meme::where('request_count', Meme::max('request_count'))->get();
        $result = $this->prefixArray($data);

        return response()->json($result);
    }

    public function store(Request $request)
    {

        $max_page = Meme::max('page');
        $page = Meme::where('page', $max_page)->count();
        $current_page = ($page <= 8) ? $max_page : $max_page + 1;

        $file = $request->file('url');
        $fileName = 'meme-' . uniqid() . '.' . $request->file('url')->extension();

        if ($file) {

            Storage::disk('public')->put($fileName, File::get($file));

            Meme::create([
                'meme_text' => $request->get('name'),
                'meme_img' => asset('uploads').'/'.$fileName,
                'page' => $current_page
            ]);


            return response('Successful create meme', 200);
        }
    }
}



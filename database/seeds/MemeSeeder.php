<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class MemeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get(public_path("/json/data.json"));
        $data = json_decode($json);
        $array = (array) $data;
        $count = 1;

        foreach ($array as $key => $obj) {


            DB::table('memes')->insert(array(
                'meme_text' => $obj->meme_text,
                'meme_img' => $obj->meme_img,
                'page' => (($key + 1) % 9 == 0) ? $count++ : $count,
                'created_at' => NOW(),
                'updated_at' => NOW(),
            ));

        }
    }
}
